<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('layout/head.php') ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <?php $this->load->view('layout/navbar.php') ?>

    <?php $this->load->view('layout/sidebar.php') ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Main content -->
      <section class="content mt-3">
        <div class="row">
          <div class="col-12">

            <div class="card card-default">

              <div class="card-header">
                <h3 class="card-title">Data Schedule</h3>
                <div class="card-tools">
                  <ul class="nav nav-pills ml-auto">
                    <li class="nav-item">
                      <a href="<?= site_url('F20117/schedule_add') ?>">
                        <button type="button" class="btn btn-secondary btn-sm"><i class="fa fa-plus-square"></i> Add Schedule</button>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>

              <div class="card-body">
                <table id="_table_" class="table table-bordered table-hover table-striped" width="100%">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Train</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>Created</th>
                    <th>Modified</th>
                    <th>File</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($data as $key => $value) { ?>
                      <tr>
                        <td><?= $key + 1 ?></td>
                        <td><?= $value['train_name'] ?></td>
                        <td><?= $value['schedule_start'] ?></td>
                        <td><?= $value['schedule_end'] ?></td>
                        <td><?= $value['schedule_created'] ?></td>
                        <td><?= $value['schedule_modified'] ?></td>
                        <td>
                          <?php 
                            if (isset($value['schedule_file']) && !empty($value['schedule_file'])) {
                              $file = explode('/', $value['schedule_file']);
                              echo $file[1];
                            }
                          ?>
                        </td>

                        <td>
                          <?php if (isset($value['schedule_file']) && !empty($value['schedule_file'])) { ?>
                            <a href="<?= base_url('uploads/'.$value['schedule_file']) ?>" data-toggle="tooltip" title="Download">
                                <button type="button" class="btn btn-success" ><i class="fas fa-file-download"></i></button>
                            </a>
                          <?php } ?>

                          <?php if ($this->session->userdata('isupdate')) { ?>
                            <a href="<?= site_url('F20117/schedule_edit/'.$value['schedule_id'])?>" data-toggle="tooltip" title="Edit">
                                <button type="button" class="btn btn-warning"><i class="fa fa-edit"></i></button>
                            </a>
                            
                          <?php } if ($this->session->userdata('isdelete')) { ?>
                            <a onclick="deletedata(<?= $value['schedule_id'] ?>)" data-toggle="tooltip" title="Delete">
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-trash"></i></button>
                            </a>
                          <?php } ?>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->

    </div>


    <?php $this->load->view('layout/footer.php') ?>

  </div>
  <!-- ./wrapper -->

  <!-- modal notification -->
  <?php $this->load->view('layout/notif.php') ?>

  <!-- modal delete -->
  <?php $this->load->view('layout/delete.php') ?>

  <?php $this->load->view('layout/javascript.php') ?>
  <script>

    var title = 'Data Train';
    var columns = [0,1,2,3,4,5,6];
    var filename = 'Export Data Train';

    function deletedata(id){
      $("#btn-delete").on("click", function(){
        window.location.href = "<?= site_url('F20117/schedule_delete/')?>"+id;
      });
    }

    $(document).ready(function(){

      <?php if (isset($notif) && !empty($notif['status']) && !empty($notif['message'])) { ?>
        $("#modal-notif").modal('show');
      <?php } ?>


      $("#_table_").DataTable({
        scrollX : true,
        columnDefs: [
            { className: "tabledata", targets: "_all"}
        ],
        dom:  '<"row"<"col-sm-12 col-md-3"l><"col-sm-12 col-md-6"B><"col-sm-12 col-md-3"f>>' +
              '<"row"<"col-sm-12"tr>>' +
              '<"row"<"col-sm-12 col-md-5"i><"col-sm-12 col-md-7"p>>',
        buttons: [
          {
            extend      : 'excel',
            title       : title,
            text        : '<i class="far fa-file-excel"></i> Excel',
            titleAttr   : 'Excel',
            className   : 'btn btn-default btn-sm',
            exportOptions: {
              columns: columns
            },
            filename: filename,
            footer: true,
          },
          {
            extend      : 'pdf',
            title       : title,
            oriented    : 'potrait',
            pageSize    : 'LEGAL',
            // download    : 'open',
            text        : '<i class="far fa-file-pdf"></i> PDF',
            titleAttr   : 'PDF',
            className   : 'btn btn-default btn-sm',
            exportOptions: {
              columns: columns
            },
            filename: filename,
            footer: true,
          },               
        ],
      });
    });
    
  </script>
</body>
</html>
