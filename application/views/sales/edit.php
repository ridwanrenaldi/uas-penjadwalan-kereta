<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('layout/head.php') ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <?php $this->load->view('layout/navbar.php') ?>

    <?php $this->load->view('layout/sidebar.php') ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Main content -->
      <section class="content mt-3">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <!-- Horizontal Form -->
              <div class="card card-default">
                <div class="card-header">
                  <h3 class="card-title">Form Input</h3>
                </div>
                
                <!-- form start -->
                <form action="<?= site_url('F20113/train_edit/'.$id) ?>" method="post" enctype="multipart/form-data">

                  <div class="card-body">

                    <div class="form-group row">
                      <label for="_user_" class="col-sm-3 col-form-label" style="text-align: right;">User</label>
                      <div class="col-sm-6">
                        <select name="_user_" id="_user_" class="form-control">
                          <option selected disabled hidden>- Choose User -</option>
                          <?php foreach ($user as $key => $value) { ?>
                            <option value="<?= $value['user_id'] ?>" <?php if($value['user_id']==$data['user_id']){echo 'selected';} ?>><?= $value['user_name'] ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>


                    <div class="form-group row">
                      <label for="_schedule_" class="col-sm-3 col-form-label" style="text-align: right;">Schedule</label>
                      <div class="col-sm-6">
                        <select name="_schedule_" id="_schedule_" class="form-control">
                          <option selected disabled hidden>- Choose Schedule -</option>
                          <?php foreach ($schedule as $key => $value) { ?>
                            <option value="<?= $value['schedule_id'] ?>" <?php if($value['schedule_id']==$data['schedule_id']){echo 'selected';} ?>><?= $value['train_name'].' | '.$value['schedule_start'].' ~ '.$value['schedule_start'] ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="_person_" class="col-sm-3 col-form-label" style="text-align: right;">Person</label>
                      <div class="col-sm-6">
                        <input type="number" name="_person_" class="form-control" id="_person_" placeholder="0" value="<?php if(set_value('_person_') != null) {echo set_value('_person_');}else{echo $data['sales_person'];} ?>" required="required" minlength="1" maxlength="11">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="_file_" class="col-sm-3 col-form-label" style="text-align: right;">File</label>
                      <div class="col-sm-6">
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" id="_file_" name="_file_" >
                              <label class="custom-file-label" for="customFile" id="_filename_" style="overflow: hidden;">Choose file</label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <div class="form-group row">
                      <div class="col-md-6 col-sm-6 offset-md-3">
                        <button type="button" class="btn btn-warning" onclick="window.history.back();">Cancel</button>
                        <button type="reset" class="btn btn-info" id="_reset_">Reset</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-footer -->
                
                </form>
              </div>
            </div>
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->

    </div>


    <?php $this->load->view('layout/footer.php') ?>

  </div>
  <!-- ./wrapper -->

  <?php $this->load->view('layout/notif.php') ?>

  <?php $this->load->view('layout/javascript.php') ?>

  <?php if (isset($notif) && !empty($notif['status']) && !empty($notif['message'])) { ?>
    <script>
      $(document).ready(function(){
          $("#modal-notif").modal('show');
      });
    </script>
  <?php } ?>

</body>
</html>
