<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Schedule extends CI_Model {
  private $table = 'schedule';
  private $primary = 'schedule_id';
  
  public function __construct(){
      parent::__construct();
      $this->load->model('M_File');
  }

  public function rules() {
    $rules = array(
      array( 'field' => '_train_',
              'label' => 'Train',
              'rules' => 'required|trim|numeric|min_length[1]|max_length[11]'),

      // array( 'field' => '_start_',
      //         'label' => 'Start',
      //         'rules' => 'required|trim'),

      // array( 'field' => '_end_',
      //         'label' => 'End',
      //         'rules' => 'required|trim'),

      array( 'field' => '_startend_',
              'label' => 'Schedule',
              'rules' => 'required|trim'),
              
    );

    return $rules;
  }

  public function getById($id){
    $this->db->from('schedule, train');
    $this->db->where('train.train_id = schedule.train_id');
    $this->db->where($this->primary, $id);
    return $this->db->get()->row_array();
  }

  public function getAll() {
    $this->db->from('schedule, train');
    $this->db->where('train.train_id = schedule.train_id');
    return $this->db->get()->result_array();
  }

  public function insert(){
    $post = $this->input->post();
    if (!empty($post)){
      if($this->session->userdata('typeusername') == 'genap'){
        $folder = 'berkas_genap';
        $uploadfile = $this->M_File->upload_file('./uploads/berkas_genap', 'pdf');

      } elseif ($this->session->userdata('typeusername') == 'ganjil') {
        $folder = 'berkas_ganjil';
        $uploadfile = $this->M_File->upload_file('./uploads/berkas_ganjil', 'doc|docx');

      } else {
        $uploadfile = array(
          'status' => 'empty',
          'message' => 'Choose file to upload'
        );
      }


      if ($uploadfile['status'] == 'error') {
        $response = $uploadfile;
      } else {
        $inputdatetime = htmlspecialchars($post['_startend_']);
        $startend = explode('  ~  ', $inputdatetime);
        
        $data = array(
          'schedule_id'           => NULL,
          'train_id'              => htmlspecialchars($post['_train_']),
          'schedule_start'        => $startend[0],
          'schedule_end'          => $startend[1],
          'schedule_created'      => date('Y-m-d H:i:s'),
          'schedule_modified'     => date('Y-m-d H:i:s'),
          'schedule_file'         => NULL,
        );

        if ($uploadfile['status'] == 'success') {
          $data['schedule_file'] = $folder.'/'.$uploadfile['data']['file_name'];
        }
  
        $data = $this->security->xss_clean($data);
        if($this->db->insert($this->table, $data)){
          $response = array(
            'status' => 'success',
            'message' => 'Success insert data',
          );
        } else {
          $response = array(
            'status' => 'error',
            'message' => 'Failed insert data',
          );
        }
      }
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data not found!',
      );
    }

    return $response;
  }

  public function update($id){
    $post = $this->input->post();
    if (!empty($post)){
      if($this->session->userdata('typeusername') == 'genap'){
        $folder = 'berkas_genap';
        $uploadfile = $this->M_File->upload_file('./uploads/berkas_genap', 'pdf');

      } elseif ($this->session->userdata('typeusername') == 'ganjil') {
        $folder = 'berkas_ganjil';
        $uploadfile = $this->M_File->upload_file('./uploads/berkas_ganjil', 'doc|docx');

      } else {
        $uploadfile = array(
          'status' => 'empty',
          'message' => 'Choose file to upload'
        );
      }

      if ($uploadfile['status'] == 'error') {
        $response = $uploadfile;

      } else {
        $inputdatetime = htmlspecialchars($post['_startend_']);
        $startend = explode('  ~  ', $inputdatetime);
        
        $data = array(
          'train_id'              => htmlspecialchars($post['_train_']),
          'schedule_start'        => $startend[0],
          'schedule_end'          => $startend[1],
          'schedule_modified'     => date('Y-m-d H:i:s'),
        );

        if ($uploadfile['status'] == 'success') {
          $data['schedule_file'] = $folder.'/'.$uploadfile['data']['file_name'];

          $getdata = $this->getById($id);
          $this->M_File->delete_file('./uploads/'.$getdata['schedule_file']);
        }
  
        $data = $this->security->xss_clean($data);
        $this->db->where($this->primary, $id);
        if($this->db->update($this->table, $data)){
          $response = array(
            'status' => 'success',
            'message' => 'Success update data',
          );
        } else {
          $response = array(
            'status' => 'error',
            'message' => 'Failed update data',
          );
        }
      }
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data not found!',
      );
    }
    return $response;
  }

  public function delete($id){
    $data = $this->getById($id);
    if ($data) {

      $path = './uploads/'.$data['schedule_file'];
      if (!empty($data['schedule_file'])) {
        $this->M_File->delete_file($path);
      }

      if($this->db->delete($this->table, array($this->primary => $id))){
        $response = array(
          'status' => 'success',
          'message' => 'Success delete data',
        );
        
      } else {
        $response = array(
          'status' => 'error',
          'message' => 'Failed delete data',
        );
      }

    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data not found',
      );
    }

    return $response;
  }

}
?>