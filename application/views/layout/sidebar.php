  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-secondary elevation-4">
    <!-- Brand Logo -->
    <a href="<?= site_url('dashboard') ?>" class="brand-link">
      <img src="<?= base_url('assets/template/dist/img/AdminLTELogo.png')?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">PT KAI</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">


      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->

          <li class="nav-item">
            <a href="<?= site_url('dashboard') ?>" class="nav-link <?php if($sidebar=='dashboard'){echo'active';}?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p> Dashboard </p>
            </a>
          </li>

          <!-- ==========[MENU KHUSUS ADMIN]========== -->
          <?php 
            $side_users = array('user_table','user_add','user_edit');
            if ($this->session->userdata('role') == 'admin') { 
          ?>
              <li class="nav-item <?php if(in_array($sidebar, $side_users)){echo 'menu-open';}?>">
                <a href="#" class="nav-link <?php if(in_array($sidebar, $side_users)){echo 'active';}?>">
                  <i class="nav-icon fa fa-user-circle"></i>
                  <p>
                    Users
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?= site_url('F20113/user_table') ?>" class="nav-link <?php if($sidebar==$side_users[0]){echo 'active';} ?>">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Table</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?= site_url('F20113/user_add') ?>" class="nav-link <?php if($sidebar==$side_users[1]){echo 'active';} ?>">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Add</p>
                    </a>
                  </li>
                </ul>
              </li>
          <?php } ?>




           <!-- ==========[MENU KHUSUS USER]========== -->
          <?php 
            $side_class = array('class_table','class_add','class_edit');
            if ($this->session->userdata('role') == 'user') { 
          ?>
              <li class="nav-item <?php if(in_array($sidebar, $side_class)){echo 'menu-open';}?>">
                <a href="#" class="nav-link <?php if(in_array($sidebar, $side_class)){echo 'active';}?>">
                  <i class="nav-icon fas fa-list-alt"></i>
                  <p>
                    Class
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?= site_url('F20113/class_table') ?>" class="nav-link <?php if($sidebar==$side_class[0]){echo 'active';} ?>">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Table</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?= site_url('F20113/class_add') ?>" class="nav-link <?php if($sidebar==$side_class[1]){echo 'active';} ?>">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Add</p>
                    </a>
                  </li>
                </ul>
              </li>


              <?php $side_train = array('train_table','train_add','train_edit') ?>
              <li class="nav-item <?php if(in_array($sidebar, $side_train)){echo 'menu-open';}?>">
                <a href="#" class="nav-link <?php if(in_array($sidebar, $side_train)){echo 'active';}?>">
                  <i class="nav-icon fas fa-subway"></i>
                  <p>
                    Train
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?= site_url('F20113/train_table') ?>" class="nav-link <?php if($sidebar==$side_train[0]){echo 'active';} ?>">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Table</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?= site_url('F20113/train_add') ?>" class="nav-link <?php if($sidebar==$side_train[1]){echo 'active';} ?>">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Add</p>
                    </a>
                  </li>
                </ul>
              </li>


              <?php $side_schedule = array('schedule_table','schedule_add','schedule_edit') ?>
              <li class="nav-item <?php if(in_array($sidebar, $side_schedule)){echo 'menu-open';}?>">
                <a href="#" class="nav-link <?php if(in_array($sidebar, $side_schedule)){echo 'active';}?>">
                  <i class="nav-icon fas fa-calendar-alt"></i>
                  <p>
                    Schedule
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?= site_url('F20117/schedule_table') ?>" class="nav-link <?php if($sidebar==$side_schedule[0]){echo 'active';} ?>">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Table</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?= site_url('F20117/schedule_add') ?>" class="nav-link <?php if($sidebar==$side_schedule[1]){echo 'active';} ?>">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Add</p>
                    </a>
                  </li>
                </ul>
              </li>


              <?php $side_sales = array('sales_table','sales_add','sales_edit') ?>
              <li class="nav-item <?php if(in_array($sidebar, $side_sales)){echo 'menu-open';}?>">
                <a href="#" class="nav-link <?php if(in_array($sidebar, $side_sales)){echo 'active';}?>">
                  <i class="nav-icon fas fa-receipt"></i>
                  <p>
                    Sales
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?= site_url('F20117/sales_table') ?>" class="nav-link <?php if($sidebar==$side_sales[0]){echo 'active';} ?>">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Table</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?= site_url('F20117/sales_add') ?>" class="nav-link <?php if($sidebar==$side_sales[1]){echo 'active';} ?>">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Add</p>
                    </a>
                  </li>
                </ul>
              </li>

          <?php } ?>
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>