<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('layout/head.php') ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <?php $this->load->view('layout/navbar.php') ?>

    <?php $this->load->view('layout/sidebar.php') ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Main content -->
      <section class="content mt-3">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <!-- Horizontal Form -->
              <div class="card card-default">
                <div class="card-header">
                  <h3 class="card-title">Form Input</h3>
                </div>
                
                <!-- form start -->
                <form action="<?= site_url('F20113/user_edit/'.$id) ?>" method="post" enctype="multipart/form-data">

                  <div class="card-body">
                    <div class="form-group row">
                      <label for="_name_" class="col-sm-3 col-form-label" style="text-align: right;">Name</label>
                      <div class="col-sm-6">
                        <input type="text" name="_name_" class="form-control" id="_name_" placeholder="Name" value="<?php if(set_value('_name_') != null) {echo set_value('_name_');}else{echo $data['user_name'];} ?>" required="required" minlength="4" maxlength="50">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="_username_" class="col-sm-3 col-form-label" style="text-align: right;">Username</label>
                      <div class="col-sm-6">

                        <div class="input-group">
                          <input type="text" name="_username_" class="form-control" id="_username_" placeholder="Username" value="<?php if(set_value('_username_') != null) {echo set_value('_username_');}else{echo $data['user_username'];} ?>" required="required" minlength="4" maxlength="20" disabled>
                          <div class="input-group-append">
                            <span class="input-group-text">
                              <div class="custom-control custom-checkbox">
                                <input class="custom-control-input checkinput" type="checkbox" id="_checkusername_">
                                <label for="_checkusername_" class="custom-control-label"></label>
                              </div>
                            </span>
                          </div>
                        </div>

                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="_email_" class="col-sm-3 col-form-label" style="text-align: right;">Email</label>
                      <div class="col-sm-6">
                        <div class="input-group">
                          <input type="text" name="_email_" class="form-control" id="_email_" placeholder="Email" value="<?php if(set_value('_email_') != null) {echo set_value('_email_');}else{echo $data['user_email'];} ?>" required="required" minlength="4" maxlength="50" disabled>
                          <div class="input-group-append">
                            <span class="input-group-text">
                              <div class="custom-control custom-checkbox">
                                <input class="custom-control-input checkinput" type="checkbox" id="_checkemail_">
                                <label for="_checkemail_" class="custom-control-label"></label>
                              </div>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="_password_" class="col-sm-3 col-form-label" style="text-align: right;">Password</label>
                      <div class="col-sm-6">
                        <div class="input-group-append">
                          <input type="password" name="_password_" class="form-control" id="_password_" placeholder="Password" value="<?= set_value('_password_'); ?>" required="required" minlength="4" maxlength="20" disabled>
                          <div class="input-group-append">
                            <span class="input-group-text">
                              <div class="custom-control custom-checkbox">
                                <input class="custom-control-input checkinput" type="checkbox" id="_checkpassword_">
                                <label for="_checkpassword_" class="custom-control-label"></label>
                              </div>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="_passconf_" class="col-sm-3 col-form-label" style="text-align: right;">Confirm Password</label>
                      <div class="col-sm-6">
                        <input type="password" name="_passconf_" class="form-control" id="_passconf_" placeholder="Confirm Password" value="<?= set_value('_passconf_'); ?>" required="required" minlength="4" maxlength="20" disabled>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="_role_" class="col-sm-3 col-form-label" style="text-align: right;">Role</label>
                      <div class="col-sm-6">
                        <select name="_role_" id="_role_" class="form-control" required="required">
                          <option selected disabled hidden>- Choose role -</option>
                          <option value="admin" <?php if(set_select('_role_', 'admin') != null) {echo set_select('_role_', 'admin');}elseif($data['user_role'] == 'admin') {echo 'selected';}?>>Admin</option>
                          <option value="user" <?php if(set_select('_role_', 'user') != null) {echo set_select('_role_', 'user');}elseif($data['user_role'] == 'user') {echo 'selected';}?>>User</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="_isupdate_" class="col-sm-3 col-form-label" style="text-align: right;">Is Update</label>
                      <div class="col-sm-6">
                        <select name="_isupdate_" id="_isupdate_" class="form-control" required="required">
                          <option selected disabled hidden>- Choose -</option>
                          <option value="1" <?php if(set_select('_isupdate_', '1') != null) {echo set_select('_isupdate_', '1');}elseif($data['user_isupdate'] == '1') {echo 'selected';}?>>True</option>
                          <option value="0" <?php if(set_select('_isupdate_', '0') != null) {echo set_select('_isupdate_', '0');}elseif($data['user_isupdate'] == '0') {echo 'selected';}?>>False</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="_isdelete_" class="col-sm-3 col-form-label" style="text-align: right;">Is Delete</label>
                      <div class="col-sm-6">
                        <select name="_isdelete_" id="_isdelete_" class="form-control" required="required">
                          <option selected disabled hidden>- Choose -</option>
                          <option value="1" <?php if(set_select('_isdelete_', '1') != null) {echo set_select('_isdelete_', '1');}elseif($data['user_isdelete'] == '1') {echo 'selected';}?>>True</option>
                          <option value="0" <?php if(set_select('_isdelete_', '0') != null) {echo set_select('_isdelete_', '0');}elseif($data['user_isdelete'] == '0') {echo 'selected';}?>>False</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="_file_" class="col-sm-3 col-form-label" style="text-align: right;">Image</label>
                      <div class="col-sm-6">
                        <div class="row">
                          <div class="col-sm-3">
                            <?php if ($data['user_image']) { ?>
                              <img src="<?= base_url('uploads/user/'.$data['user_image'])?>" id="img-preview" alt="Preview Image" class="img-thumbnail">
                            <?php } else { ?>
                              <img src="<?= base_url('assets/images/default.png')?>" id="img-preview" alt="Preview Image" class="img-thumbnail">
                            <?php } ?>
                          </div>
                          <div class="col-sm-9">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" id="_file_" name="_file_" accept="image/*">
                              <label class="custom-file-label" for="customFile" id="_filename_" style="overflow: hidden;">Choose file</label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <div class="form-group row">
                      <div class="col-md-6 col-sm-6 offset-md-3">
                        <button type="button" class="btn btn-warning" onclick="window.history.back();">Cancel</button>
                        <button type="reset" class="btn btn-info" id="_reset_">Reset</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-footer -->
                
                </form>
              </div>
            </div>
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->

    </div>


    <?php $this->load->view('layout/footer.php') ?>

  </div>
  <!-- ./wrapper -->

  <?php $this->load->view('layout/notif.php') ?>

  <?php $this->load->view('layout/javascript.php') ?>

    <script>
      $(document).ready(function(){
        <?php if (isset($notif) && !empty($notif['status']) && !empty($notif['message'])) { ?>
          $("#modal-notif").modal('show');
        <?php } ?>

        $('.checkinput').on('change', function(){
          var id = $(this).attr('id');

          if (id == '_checkusername_') {
            if ($(this).is(':checked')) {
              $('#_username_').prop('disabled', false);
            } else {
              $('#_username_').prop('disabled', true);
            }

          } else if (id == '_checkemail_'){
            if ($(this).is(':checked')) {
              $('#_email_').prop('disabled', false);
            } else {
              $('#_email_').prop('disabled', true);
            }

          } else if (id == '_checkpassword_' || id == '_checkpassconf_'){
            if ($(this).is(':checked')) {
              $('#_password_').prop('disabled', false );
              $('#_passconf_').prop('disabled', false );
            } else {
              $('#_password_').prop('disabled', true );
              $('#_passconf_').prop('disabled', true );
            }
          }
        });
      });
    </script>

</body>
</html>
