  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">


      <li class="nav-item dropdown user-menu border-left">
        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
          <?php if ($this->session->userdata('image')) { ?>
            <img src="<?= base_url('uploads/user/'.$this->session->userdata('image')) ?>" class="user-image img-circle elevation-2" alt="User Image">
          <?php } else { ?>
            <img src="<?= base_url('assets/images/default.png') ?>" class="user-image img-circle elevation-2" alt="User Image">
          <?php } ?>
          <span class="d-none d-md-inline"><?= $this->session->userdata('username') ?></span>
        </a>
        <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">


          <!-- Menu Footer-->
          <li class="user-footer">
            <div class="row">
              <div class="col-md-12">
                <a href="<?= site_url('auth/logout') ?>" class="btn btn-info btn-block"><i class="fas fa-sign-out-alt"></i> Logout</a>
              </div>
            </div>
          </li>
        </ul>
      </li>

    </ul>
  </nav>
  <!-- /.navbar -->