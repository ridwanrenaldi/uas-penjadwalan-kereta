<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('layout/head.php') ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <?php $this->load->view('layout/navbar.php') ?>

    <?php $this->load->view('layout/sidebar.php') ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Main content -->
      <section class="content mt-3">
        <div class="row">
          <div class="col-12">

            <div class="card card-default">

              <div class="card-header">
                <h3 class="card-title">Data Class</h3>
                <div class="card-tools">
                  <ul class="nav nav-pills ml-auto">
                    <li class="nav-item">
                      <a href="<?= site_url('F20113/class_add') ?>">
                        <button type="button" class="btn btn-secondary btn-sm"><i class="fa fa-plus-square"></i> Add Class</button>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>

              <div class="card-body">
                <table id="_table_" class="table table-bordered table-hover table-striped" width="100%">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>File</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($data as $key => $value) { ?>
                      <tr>
                        <td><?= $key + 1 ?></td>
                        <td><?= $value['class_name'] ?></td>
                        <td><?= $value['class_description'] ?></td>
                        <td>
                          <?php 
                            if (isset($value['class_file']) && !empty($value['class_file'])) {
                              $file = explode('/', $value['class_file']);
                              echo $file[1];
                            }
                          ?>
                        </td>
                        
                        <td>
                          <?php if (isset($value['class_file']) && !empty($value['class_file'])) { ?>
                            <a href="<?= base_url('uploads/'.$value['class_file']) ?>" data-toggle="tooltip" title="Download">
                                <button type="button" class="btn btn-success" ><i class="fas fa-file-download"></i></button>
                            </a>
                          <?php } ?>

                          <?php if ($this->session->userdata('isupdate')) { ?>
                            <a href="<?= site_url('F20113/class_edit/'.$value['class_id'])?>" data-toggle="tooltip" title="Edit">
                                <button type="button" class="btn btn-warning"><i class="fa fa-edit"></i></button>
                            </a>

                          <?php } if ($this->session->userdata('isdelete')) { ?>
                            <a onclick="deletedata(<?= $value['class_id'] ?>)" data-toggle="tooltip" title="Delete">
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-trash"></i></button>
                            </a>
                          <?php } ?>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->

    </div>


    <?php $this->load->view('layout/footer.php') ?>

  </div>
  <!-- ./wrapper -->

  <!-- modal notification -->
  <?php $this->load->view('layout/notif.php') ?>

  <!-- modal delete -->
  <?php $this->load->view('layout/delete.php') ?>

  <?php $this->load->view('layout/javascript.php') ?>
  <script>

    var title = 'Data Class';
    var columns = [0,1,2,3];
    var filename = 'Export Data Class';

    function deletedata(id){
      $("#btn-delete").on("click", function(){
        window.location.href = "<?= site_url('F20113/class_delete/')?>"+id;
      });
    }

    $(document).ready(function(){

      <?php if (isset($notif) && !empty($notif['status']) && !empty($notif['message'])) { ?>
        $("#modal-notif").modal('show');
      <?php } ?>


      $("#_table_").DataTable({
        scrollX : true,
        columnDefs: [
            { className: "tabledata", targets: "_all"}
        ],
        dom:  '<"row"<"col-sm-12 col-md-3"l><"col-sm-12 col-md-6"B><"col-sm-12 col-md-3"f>>' +
              '<"row"<"col-sm-12"tr>>' +
              '<"row"<"col-sm-12 col-md-5"i><"col-sm-12 col-md-7"p>>',
        buttons: [
          {
            extend      : 'excel',
            title       : title,
            text        : '<i class="far fa-file-excel"></i> Excel',
            titleAttr   : 'Excel',
            className   : 'btn btn-default btn-sm',
            exportOptions: {
              columns: columns
            },
            filename: filename,
            footer: true,
          },
          {
            extend      : 'pdf',
            title       : title,
            oriented    : 'potrait',
            pageSize    : 'LEGAL',
            // download    : 'open',
            text        : '<i class="far fa-file-pdf"></i> PDF',
            titleAttr   : 'PDF',
            className   : 'btn btn-default btn-sm',
            exportOptions: {
              columns: columns
            },
            filename: filename,
            footer: true,
          },               
        ],
      });
    });
    
  </script>
</body>
</html>
