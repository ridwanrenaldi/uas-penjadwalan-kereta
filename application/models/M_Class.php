<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Class extends CI_Model {
  private $table = 'class';
  private $primary = 'class_id';
  
  public function __construct(){
      parent::__construct();
      $this->load->model('M_File');
  }

  public function rules() {
    $rules = array(
      array( 'field' => '_name_',
              'label' => 'Name',
              'rules' => 'required|trim|alpha_numeric_spaces|min_length[4]|max_length[20]'),

      array( 'field' => '_description_',
              'label' => 'Description',
              'rules' => 'required|trim')
    );

    return $rules;
  }

  public function getById($id){
    return $this->db->get_where($this->table, array($this->primary => $id) )->row_array();
  }

  public function getAll() {
    return $this->db->get($this->table)->result_array();
  }

  public function insert(){
    $post = $this->input->post();
    if (!empty($post)){

      if($this->session->userdata('typeusername') == 'genap'){
        $folder = 'berkas_genap';
        $uploadfile = $this->M_File->upload_file('./uploads/berkas_genap', 'pdf');

      } elseif ($this->session->userdata('typeusername') == 'ganjil') {
        $folder = 'berkas_ganjil';
        $uploadfile = $this->M_File->upload_file('./uploads/berkas_ganjil', 'doc|docx');

      } else {
        $uploadfile = array(
          'status' => 'empty',
          'message' => 'Choose file to upload'
        );
      }


      if ($uploadfile['status'] == 'error') {
        $response = $uploadfile;
      } else {
        $data = array(
          'class_id'           => NULL,
          'class_name'         => htmlspecialchars($post['_name_']),
          'class_description'  => htmlspecialchars($post['_description_']),
          'class_file'         => NULL,
        );

        if ($uploadfile['status'] == 'success') {
          $data['class_file'] = $folder.'/'.$uploadfile['data']['file_name'];
        }
  
        $data = $this->security->xss_clean($data);
        if($this->db->insert($this->table, $data)){
          $response = array(
            'status' => 'success',
            'message' => 'Success insert data',
          );

        } else {
          $response = array(
            'status' => 'error',
            'message' => 'Failed insert data',
          );
        }
      }

    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data not found!',
      );
    }

    return $response;
  }

  public function update($id){
    $post = $this->input->post();
    if (!empty($post)){
      
      if($this->session->userdata('typeusername') == 'genap'){
        $folder = 'berkas_genap';
        $uploadfile = $this->M_File->upload_file('./uploads/berkas_genap', 'pdf');

      } elseif ($this->session->userdata('typeusername') == 'ganjil') {
        $folder = 'berkas_ganjil';
        $uploadfile = $this->M_File->upload_file('./uploads/berkas_ganjil', 'doc|docx');

      } else {
        $uploadfile = array(
          'status' => 'empty',
          'message' => 'Choose file to upload'
        );
      }

      if ($uploadfile['status'] == 'error') {
        $response = $uploadfile;

      } else {
        $data = array(
          'class_name'         => htmlspecialchars($post['_name_']),
          'class_description'  => htmlspecialchars($post['_description_']),
        );

        if ($uploadfile['status'] == 'success') {
          $data['class_file'] = $folder.'/'.$uploadfile['data']['file_name'];

          $getdata = $this->getById($id);
          $this->M_File->delete_file('./uploads/'.$getdata['class_file']);
        }
  
        $data = $this->security->xss_clean($data);
        $this->db->where($this->primary, $id);
        if($this->db->update($this->table, $data)){
          $response = array(
            'status' => 'success',
            'message' => 'Success update data',
          );
        } else {
          $response = array(
            'status' => 'error',
            'message' => 'Failed update data',
          );
        }
      }

    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data not found!',
      );
    }
    return $response;
  }

  public function delete($id){
    $data = $this->getById($id);
    if ($data) {

      $path = './uploads/'.$data['class_file'];
      if (!empty($data['class_file'])) {
        $this->M_File->delete_file($path);
      }

      if($this->db->delete($this->table, array($this->primary => $id))){
        $response = array(
          'status' => 'success',
          'message' => 'Success delete data',
        );

      } else {
        $response = array(
          'status' => 'error',
          'message' => 'Failed delete data',
        );
      }

    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data not found',
      );
    }

    return $response;
  }

}
?>