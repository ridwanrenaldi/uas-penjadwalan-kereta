<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F20117 extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_Auth');
    $this->load->model('M_Schedule');
    $this->load->model('M_Train');
    $this->load->model('M_Sales');
    $this->load->model('M_User');
	}


  // ============================================================
  //                            SCHEDULE
  // ============================================================
  public function schedule_table()
	{
    $sess = $this->M_Auth->check_session(array('user'));
    if ($sess === FALSE) {
      redirect(site_url('auth/logout'),'refresh');

    } else {
      if ($sess['username'] != 'f20117') {
        $this->load->view('auth/access_denied');

      } else {
        $data['sidebar'] = 'schedule_table';
        $data['data'] = $this->M_Schedule->getAll();
  
        if ($this->session->flashdata('notif')) {
          $data['notif'] = $this->session->flashdata('notif');
        }
        $this->load->view('schedule/table', $data);
      }
    }
	}

  public function schedule_add()
	{
    $sess = $this->M_Auth->check_session(array('user'));
    if ($sess === FALSE) {
      redirect(site_url('auth/logout'),'refresh');

    } else {
      if ($sess['username'] != 'f20117') {
        $this->load->view('auth/access_denied');

      } else {
        $data['sidebar'] = 'schedule_add'; 
        $data['train'] = $this->M_Train->getAll();
        $this->form_validation->set_rules($this->M_Schedule->rules());
  
        if ($this->form_validation->run() === TRUE) {
          $insert =  $this->M_Schedule->insert();
          $this->session->set_flashdata('notif', $insert);
          redirect(site_url('F20117/schedule_add'),'refresh');
    
        } else {
          $data['notif'] = array('status' => 'error', 'message' => str_replace("\n", '', validation_errors('<li>','</li>')));
          if ($this->session->flashdata('notif')) {
            $data['notif'] = $this->session->flashdata('notif');
          }
          
          $this->load->view('schedule/add', $data);
        }
      }
    }
	}

  public function schedule_edit($id=null)
	{
    $sess = $this->M_Auth->check_session(array('user'));
    if ($sess === FALSE) {
      redirect(site_url('auth/logout'),'refresh');

    } else {
      if ($sess['username'] != 'f20117') {
        $this->load->view('auth/access_denied');

      } else {
        $data['sidebar'] = 'schedule_edit';
        $data['data'] = $this->M_Schedule->getById($id);
        $data['train'] = $this->M_Train->getAll();
        $data['id'] = $id;
    
        if ($id != null && $data['data']) {
    
          $this->form_validation->set_rules($this->M_Schedule->rules());
          if ($this->form_validation->run() === TRUE) {
            $update =  $this->M_Schedule->update($id);
            $this->session->set_flashdata('notif', $update);
            redirect(site_url('F20117/schedule_edit/'.$id),'refresh');
    
          } else {
            $data['notif'] = array('status' => 'error', 'message' => str_replace("\n", '', validation_errors('<li>','</li>')));
            if ($this->session->flashdata('notif')) {
              $data['notif'] = $this->session->flashdata('notif');
            }
            $this->load->view('schedule/edit', $data);
          }
    
        } else {
          redirect(site_url('F20117/schedule_table'),'refresh');
        }
      }
    }
	}

  public function schedule_delete($id=null)
	{
    $sess = $this->M_Auth->check_session(array('user'));
    if ($sess === FALSE) {
      redirect(site_url('auth/logout'),'refresh');

    } else {
      if ($sess['username'] != 'f20117') {
        $this->load->view('auth/access_denied');

      } else {
        $data = $this->M_Schedule->getById($id);
        if ($id != null && $data) {
          echo 'Wait a minute...';
          $delete =  $this->M_Schedule->delete($id);
          $this->session->set_flashdata('notif', $delete);
          redirect(site_url('F20117/schedule_table'),'refresh');
    
        } else {
          redirect(site_url('F20117/schedule_table'),'refresh');
        }
      }
    }
	}


  // ============================================================
  //                            SALES
  // ============================================================
  public function sales_table()
	{
    $sess = $this->M_Auth->check_session(array('user'));
    if ($sess === FALSE) {
      redirect(site_url('auth/logout'),'refresh');

    } else {
      if ($sess['username'] != 'f20117') {
        $this->load->view('auth/access_denied');

      } else {
        $data['sidebar'] = 'sales_table';
        $data['data'] = $this->M_Sales->getAll();
  
        if ($this->session->flashdata('notif')) {
          $data['notif'] = $this->session->flashdata('notif');
        }
        $this->load->view('sales/table', $data);
      }
    }
	}

  public function sales_add()
	{
    $sess = $this->M_Auth->check_session(array('user'));
    if ($sess === FALSE) {
      redirect(site_url('auth/logout'),'refresh');

    } else {
      if ($sess['username'] != 'f20117') {
        $this->load->view('auth/access_denied');

      } else {
        $data['sidebar'] = 'sales_add'; 
        $data['user'] = $this->M_User->getAll();
        $data['schedule'] = $this->M_Schedule->getAll();
        $this->form_validation->set_rules($this->M_Sales->rules());
  
        if ($this->form_validation->run() === TRUE) {
          $insert =  $this->M_Sales->insert();
          $this->session->set_flashdata('notif', $insert);
          redirect(site_url('F20117/sales_add'),'refresh');
    
        } else {
          $data['notif'] = array('status' => 'error', 'message' => str_replace("\n", '', validation_errors('<li>','</li>')));
          if ($this->session->flashdata('notif')) {
            $data['notif'] = $this->session->flashdata('notif');
          }
          
          $this->load->view('sales/add', $data);
        }
      }
    }
	}

  public function sales_edit($id=null)
	{
    $sess = $this->M_Auth->check_session(array('user'));
    if ($sess === FALSE) {
      redirect(site_url('auth/logout'),'refresh');

    } else {
      if ($sess['username'] != 'f20117') {
        $this->load->view('auth/access_denied');

      } else {
        $data['sidebar'] = 'sales_edit';
        $data['data'] = $this->M_Sales->getById($id);
        $data['user'] = $this->M_User->getAll();
        $data['schedule'] = $this->M_Schedule->getAll();
        $data['id'] = $id;
    
        if ($id != null && $data['data']) {
    
          $this->form_validation->set_rules($this->M_Sales->rules());
          if ($this->form_validation->run() === TRUE) {
            $update =  $this->M_Sales->update($id);
            $this->session->set_flashdata('notif', $update);
            redirect(site_url('F20117/sales_edit/'.$id),'refresh');
    
          } else {
            $data['notif'] = array('status' => 'error', 'message' => str_replace("\n", '', validation_errors('<li>','</li>')));
            if ($this->session->flashdata('notif')) {
              $data['notif'] = $this->session->flashdata('notif');
            }
            $this->load->view('sales/edit', $data);
          }
    
        } else {
          redirect(site_url('F20117/sales_table'),'refresh');
        }
      }
    }
	}

  public function sales_delete($id=null)
	{
    $sess = $this->M_Auth->check_session(array('user'));
    if ($sess === FALSE) {
      redirect(site_url('auth/logout'),'refresh');

    } else {
      if ($sess['username'] != 'f20117') {
        $this->load->view('auth/access_denied');

      } else {
        $data = $this->M_Sales->getById($id);
        if ($id != null && $data) {
          echo 'Wait a minute...';
          $delete =  $this->M_Sales->delete($id);
          $this->session->set_flashdata('notif', $delete);
          redirect(site_url('F20117/sales_table'),'refresh');
    
        } else {
          redirect(site_url('F20117/sales_table'),'refresh');
        }
      }
    }
	}

}