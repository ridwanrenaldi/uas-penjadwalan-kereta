<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
  {
    parent::__construct();
    $this->load->model('M_Auth');
	}

	public function index()
	{
		if ($this->session->userdata('username')) {
			redirect(site_url('dashboard/index'),'refresh');
		} else {

			$this->form_validation->set_rules($this->M_Auth->rules_login());
      if ($this->form_validation->run() === TRUE) {
				
				$login = $this->M_Auth->login();
				if ($login['status'] == 'success') {

					if ($login['username'] == 'f20113') {
						redirect(site_url('F20113/class_table'),'refresh');

					} elseif ($login['username'] == 'f20117') {
						redirect(site_url('F20117/schedule_table'),'refresh');

					} else {
						redirect(site_url('dashboard/index'),'refresh');
					}
					
				} else {
					$this->session->set_flashdata('notif', $login);
      		redirect(site_url('auth/index'),'refresh');
				}

			} else {
				$data['notif'] = array('status' => 'error', 'message' => '');
				if ($this->session->flashdata('notif')) {
					$data['notif'] = $this->session->flashdata('notif');
				}
				$this->load->view('auth/login', $data);
			}
		}
	}

	public function register(){
		if ($this->session->userdata('username')) {
			redirect(site_url('dashboard/index'),'refresh');
		} else {

			$this->form_validation->set_rules($this->M_Auth->rules_register());
      if ($this->form_validation->run() === TRUE) {
				$register = $this->M_Auth->register();

				if ($register['status'] == 'success') {
					$this->session->set_flashdata('notif', $register);
					redirect(site_url('auth/index'),'refresh');
					
				} else {
					$this->session->set_flashdata('notif', $register);
      		redirect(site_url('auth/register'),'refresh');
				}

			} else {
				$data['notif'] = array('status' => 'error', 'message' => str_replace("\n", '', validation_errors('<li>','</li>')));
				if ($this->session->flashdata('notif')) {
					$data['notif'] = $this->session->flashdata('notif');
				}
				$this->load->view('auth/register', $data);
			}
		}
	}

	public function logout()
	{
    $this->session->sess_destroy();
    redirect(site_url('auth/index'),'refresh');
  }
}
