<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('layout/head.php') ?>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card card-outline card-default">
    <div class="card-header text-center">
      <a href="../../index2.html" class="h1"><b>PT KAI</b></a>
    </div>
    <div class="card-body">
      <p class="login-box-msg">Silahkan Login Terlebih Dahulu</p>

      <form action="<?= site_url('auth/index') ?>" method="post">

        <div class="input-group mb-3">
          <input type="text" name="_username_" class="form-control" placeholder="Username">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>

        <div class="input-group mb-3">
          <input type="password" name="_password_" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>

        <div class="row">
          <!-- /.col -->
          <div class="col-12">
            <button type="submit" class="btn btn-default btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>


        <p class="mb-0">
          <a href="<?= site_url('auth/register') ?>" class="text-center">Register a new membership</a>
        </p>

      </form>

    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.login-box -->
<?php $this->load->view('layout/notif.php') ?>

<?php $this->load->view('layout/javascript.php') ?>

<?php if (isset($notif) && !empty($notif['status']) && !empty($notif['message'])) { ?>
  <script>
    $(document).ready(function(){
      $("#modal-notif").modal('show');
    });
  </script>
<?php } ?>

</body>
</html>
