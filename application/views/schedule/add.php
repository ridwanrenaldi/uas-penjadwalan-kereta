<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('layout/head.php') ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <?php $this->load->view('layout/navbar.php') ?>

    <?php $this->load->view('layout/sidebar.php') ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Main content -->
      <section class="content mt-3">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <!-- Horizontal Form -->
              <div class="card card-default">
                <div class="card-header">
                  <h3 class="card-title">Form Input</h3>
                </div>

                <!-- form start -->
                <form action="<?= site_url('F20117/schedule_add') ?>" method="post" enctype="multipart/form-data">

                  <div class="card-body">

                    <div class="form-group row">
                      <label for="_train_" class="col-sm-3 col-form-label" style="text-align: right;">Train</label>
                      <div class="col-sm-6">
                        <select name="_train_" id="_train_" class="form-control">
                          <option selected disabled hidden>- Choose Train -</option>
                          <?php foreach ($train as $key => $value) { ?>
                            <option value="<?= $value['train_id'] ?>"><?= $value['train_name'] ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="_startend_" class="col-sm-3 col-form-label" style="text-align: right;">Schedule</label>
                      <div class="col-sm-6">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-clock"></i></span>
                          </div>
                          <input type="text" name="_startend_" class="form-control float-right" id="reservationtime"  value="<?= set_value('_startend_'); ?>">
                        </div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="_file_" class="col-sm-3 col-form-label" style="text-align: right;">File</label>
                      <div class="col-sm-6">
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" id="_file_" name="_file_" >
                              <label class="custom-file-label" for="customFile" id="_filename_" style="overflow: hidden;">Choose file</label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <div class="form-group row">
                      <div class="col-md-6 col-sm-6 offset-md-3">
                        <button type="button" class="btn btn-warning" onclick="window.history.back();">Cancel</button>
                        <button type="reset" class="btn btn-info" id="_reset_">Reset</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-footer -->
                
                </form>
              </div>
            </div>
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->

    </div>


    <?php $this->load->view('layout/footer.php') ?>

  </div>
  <!-- ./wrapper -->

  <?php $this->load->view('layout/notif.php') ?>

  <?php $this->load->view('layout/javascript.php') ?>

  <script>
    $(document).ready(function(){
      <?php if (isset($notif) && !empty($notif['status']) && !empty($notif['message'])) { ?>
        $("#modal-notif").modal('show');
      <?php } ?>

      $('#reservationtime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 1,
        showDropdowns: true,
        locale: {
          format: 'YYYY-MM-DD HH:mm:ss',
          separator: '  ~  ',
        },
      });

    });
  </script>

</body>
</html>
