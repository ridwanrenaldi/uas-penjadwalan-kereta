<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F20113 extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_Auth');
    $this->load->model('M_User');
    $this->load->model('M_Class');
    $this->load->model('M_Train');
	}

  // ============================================================
  //                            USER
  // ============================================================
	public function user_table()
	{
    $sess = $this->M_Auth->check_session(array('admin'));
    if ($sess === FALSE) {
      redirect(site_url('auth/logout'),'refresh');

    } else {
      $data['sidebar'] = 'user_table';
      $data['data'] = $this->M_User->getAll();

      if ($this->session->flashdata('notif')) {
        $data['notif'] = $this->session->flashdata('notif');
      }
      $this->load->view('user/table', $data);
    }
	}

  public function user_add()
	{
    $sess = $this->M_Auth->check_session(array('admin'));
    if ($sess === FALSE) {
      redirect(site_url('auth/logout'),'refresh');

    } else {
      $data['sidebar'] = 'user_add'; 
      $this->form_validation->set_rules($this->M_User->rules());

      if ($this->form_validation->run() === TRUE) {
        $insert =  $this->M_User->insert();
        $this->session->set_flashdata('notif', $insert);
        redirect(site_url('F20113/user_add'),'refresh');
  
      } else {
        $data['notif'] = array('status' => 'error', 'message' => str_replace("\n", '', validation_errors('<li>','</li>')));
        if ($this->session->flashdata('notif')) {
          $data['notif'] = $this->session->flashdata('notif');
        }
        
        $this->load->view('user/add', $data);
      }
    }
	}

  public function user_edit($id=null)
	{
    $sess = $this->M_Auth->check_session(array('admin'));
    if ($sess === FALSE) {
      redirect(site_url('auth/logout'),'refresh');

    } else {
      $data['sidebar'] = 'user_edit';
      $data['data'] = $this->M_User->getById($id);
      $data['id'] = $id;
  
      if ($id != null && $data['data']) {
  
        $this->form_validation->set_rules($this->M_User->rules());
        if ($this->form_validation->run() === TRUE) {
          $update =  $this->M_User->update($id);
          $this->session->set_flashdata('notif', $update);
          redirect(site_url('F20113/user_edit/'.$id),'refresh');
  
        } else {
          $data['notif'] = array('status' => 'error', 'message' => str_replace("\n", '', validation_errors('<li>','</li>')));
          if ($this->session->flashdata('notif')) {
            $data['notif'] = $this->session->flashdata('notif');
          }
          $this->load->view('user/edit', $data);
        }
  
      } else {
        redirect(site_url('F20113/user_table'),'refresh');
      }
    }
	}

  public function user_delete($id=null)
	{
    $sess = $this->M_Auth->check_session(array('admin'));
    if ($sess === FALSE) {
      redirect(site_url('auth/logout'),'refresh');

    } else {
      $data = $this->M_User->getById($id);
      if ($id != null && $data) {
        echo 'Wait a minute...';
        $delete =  $this->M_User->delete($id);
        $this->session->set_flashdata('notif', $delete);
        redirect(site_url('F20113/user_table'),'refresh');
  
      } else {
        redirect(site_url('F20113/user_table'),'refresh');
      }
    }
	}




  // ============================================================
  //                            Class
  // ============================================================
  public function class_table()
	{
    $sess = $this->M_Auth->check_session(array('user'));
    if ($sess === FALSE) {
      redirect(site_url('auth/logout'),'refresh');

    } else {
      if ($sess['username'] != 'f20113') {
        $this->load->view('auth/access_denied');

      } else {
        $data['sidebar'] = 'class_table';
        $data['data'] = $this->M_Class->getAll();
  
        if ($this->session->flashdata('notif')) {
          $data['notif'] = $this->session->flashdata('notif');
        }
        $this->load->view('class/table', $data);
      }

    }
	}

  public function class_add()
	{
    $sess = $this->M_Auth->check_session(array('user'));
    if ($sess === FALSE) {
      redirect(site_url('auth/logout'),'refresh');

    } else {
      if ($sess['username'] != 'f20113') {
        $this->load->view('auth/access_denied');

      } else {
        $data['sidebar'] = 'class_add'; 
        $this->form_validation->set_rules($this->M_Class->rules());
  
        if ($this->form_validation->run() === TRUE) {
          $insert =  $this->M_Class->insert();
          $this->session->set_flashdata('notif', $insert);
          redirect(site_url('F20113/class_add'),'refresh');
    
        } else {
          $data['notif'] = array('status' => 'error', 'message' => str_replace("\n", '', validation_errors('<li>','</li>')));
          if ($this->session->flashdata('notif')) {
            $data['notif'] = $this->session->flashdata('notif');
          }
          
          $this->load->view('class/add', $data);
        }
      }
    }
	}

  public function class_edit($id=null)
	{
    $sess = $this->M_Auth->check_session(array('user'));
    if ($sess === FALSE) {
      redirect(site_url('auth/logout'),'refresh');

    } else {
      if ($sess['username'] != 'f20113') {
        $this->load->view('auth/access_denied');

      } else {
        $data['sidebar'] = 'class_edit';
        $data['data'] = $this->M_Class->getById($id);
        $data['id'] = $id;
    
        if ($id != null && $data['data']) {
    
          $this->form_validation->set_rules($this->M_Class->rules());
          if ($this->form_validation->run() === TRUE) {
            $update =  $this->M_Class->update($id);
            $this->session->set_flashdata('notif', $update);
            redirect(site_url('F20113/class_edit/'.$id),'refresh');
    
          } else {
            $data['notif'] = array('status' => 'error', 'message' => str_replace("\n", '', validation_errors('<li>','</li>')));
            if ($this->session->flashdata('notif')) {
              $data['notif'] = $this->session->flashdata('notif');
            }
            $this->load->view('class/edit', $data);
          }
    
        } else {
          redirect(site_url('F20113/class_table'),'refresh');
        }
      }
    }
	}

  public function class_delete($id=null)
	{
    $sess = $this->M_Auth->check_session(array('user'));
    if ($sess === FALSE) {
      redirect(site_url('auth/logout'),'refresh');

    } else {
      if ($sess['username'] != 'f20113') {
        $this->load->view('auth/access_denied');

      } else {
        $data = $this->M_Class->getById($id);

        if ($id != null && $data) {
          echo 'Wait a minute...';
          $delete =  $this->M_Class->delete($id);
          $this->session->set_flashdata('notif', $delete);
          redirect(site_url('F20113/class_table'),'refresh');
    
        } else {
          redirect(site_url('F20113/class_table'),'refresh');
        }
      }
    }
	}




  // ============================================================
  //                            TRAIN
  // ============================================================
  public function train_table()
	{
    $sess = $this->M_Auth->check_session(array('user'));
    if ($sess === FALSE) {
      redirect(site_url('auth/logout'),'refresh');

    } else {
      if ($sess['username'] != 'f20113') {
        $this->load->view('auth/access_denied');

      } else {
        $data['sidebar'] = 'train_table';
        $data['data'] = $this->M_Train->getAll();
  
        if ($this->session->flashdata('notif')) {
          $data['notif'] = $this->session->flashdata('notif');
        }
        $this->load->view('train/table', $data);
      }
    }
	}

  public function train_add()
	{
    $sess = $this->M_Auth->check_session(array('user'));
    if ($sess === FALSE) {
      redirect(site_url('auth/logout'),'refresh');

    } else {
      if ($sess['username'] != 'f20113') {
        $this->load->view('auth/access_denied');

      } else {
        $data['sidebar'] = 'train_add'; 
        $data['class'] = $this->M_Class->getAll();
        $this->form_validation->set_rules($this->M_Train->rules());
  
        if ($this->form_validation->run() === TRUE) {
          $insert =  $this->M_Train->insert();
          $this->session->set_flashdata('notif', $insert);
          redirect(site_url('F20113/train_add'),'refresh');
    
        } else {
          $data['notif'] = array('status' => 'error', 'message' => str_replace("\n", '', validation_errors('<li>','</li>')));
          if ($this->session->flashdata('notif')) {
            $data['notif'] = $this->session->flashdata('notif');
          }
          
          $this->load->view('train/add', $data);
        }
      }
    }
	}

  public function train_edit($id=null)
	{
    $sess = $this->M_Auth->check_session(array('user'));
    if ($sess === FALSE) {
      redirect(site_url('auth/logout'),'refresh');

    } else {
      if ($sess['username'] != 'f20113') {
        $this->load->view('auth/access_denied');

      } else {
        $data['sidebar'] = 'train_edit';
        $data['data'] = $this->M_Train->getById($id);
        $data['class'] = $this->M_Class->getAll();
        $data['id'] = $id;
    
        if ($id != null && $data['data']) {
    
          $this->form_validation->set_rules($this->M_Train->rules());
          if ($this->form_validation->run() === TRUE) {
            $update =  $this->M_Train->update($id);
            $this->session->set_flashdata('notif', $update);
            redirect(site_url('F20113/train_edit/'.$id),'refresh');
    
          } else {
            $data['notif'] = array('status' => 'error', 'message' => str_replace("\n", '', validation_errors('<li>','</li>')));
            if ($this->session->flashdata('notif')) {
              $data['notif'] = $this->session->flashdata('notif');
            }
            $this->load->view('train/edit', $data);
          }
    
        } else {
          redirect(site_url('F20113/train_table'),'refresh');
        }
      }
    }
	}

  public function train_delete($id=null)
	{
    $sess = $this->M_Auth->check_session(array('user'));
    if ($sess === FALSE) {
      redirect(site_url('auth/logout'),'refresh');

    } else {
      if ($sess['username'] != 'f20113') {
        $this->load->view('auth/access_denied');

      } else {
        $data = $this->M_Train->getById($id);
        if ($id != null && $data) {
          echo 'Wait a minute...';
          $delete =  $this->M_Train->delete($id);
          $this->session->set_flashdata('notif', $delete);
          redirect(site_url('F20113/train_table'),'refresh');
    
        } else {
          redirect(site_url('F20113/train_table'),'refresh');
        }
      }
    }
	}
}
