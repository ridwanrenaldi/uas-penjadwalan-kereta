-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Waktu pembuatan: 06 Jul 2021 pada 22.24
-- Versi server: 10.4.10-MariaDB
-- Versi PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uas_web_lima`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `class`
--

DROP TABLE IF EXISTS `class`;
CREATE TABLE IF NOT EXISTS `class` (
  `class_id` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(100) NOT NULL,
  `class_description` text NOT NULL,
  `class_file` varchar(250) NOT NULL,
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sales`
--

DROP TABLE IF EXISTS `sales`;
CREATE TABLE IF NOT EXISTS `sales` (
  `sales_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `sales_person` int(11) NOT NULL,
  `sales_price` int(11) NOT NULL,
  `sales_total` int(11) NOT NULL,
  `sales_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `sales_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sales_file` varchar(250) NOT NULL,
  PRIMARY KEY (`sales_id`),
  KEY `user_id` (`user_id`),
  KEY `schedule_id` (`schedule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `schedule`
--

DROP TABLE IF EXISTS `schedule`;
CREATE TABLE IF NOT EXISTS `schedule` (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT,
  `train_id` int(11) NOT NULL,
  `schedule_start` datetime NOT NULL,
  `schedule_end` datetime NOT NULL,
  `schedule_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `schedule_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `schedule_file` varchar(250) NOT NULL,
  PRIMARY KEY (`schedule_id`),
  KEY `train_id` (`train_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `train`
--

DROP TABLE IF EXISTS `train`;
CREATE TABLE IF NOT EXISTS `train` (
  `train_id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL,
  `train_name` varchar(250) NOT NULL,
  `train_route` varchar(250) NOT NULL,
  `train_price` int(11) NOT NULL,
  `train_file` varchar(250) NOT NULL,
  PRIMARY KEY (`train_id`),
  KEY `class_id` (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
  `user_username` varchar(20) NOT NULL,
  `user_email` varchar(250) NOT NULL,
  `user_password` varchar(250) NOT NULL,
  `user_isactive` tinyint(1) NOT NULL,
  `user_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_role` enum('admin','user') NOT NULL,
  `user_image` varchar(250) NOT NULL,
  `user_isupdate` tinyint(1) NOT NULL,
  `user_isdelete` tinyint(1) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_username`, `user_email`, `user_password`, `user_isactive`, `user_created`, `user_modified`, `user_role`, `user_image`, `user_isupdate`, `user_isdelete`) VALUES
(1, 'Admin', 'admin', 'admin@gmail.com', '$2y$08$87g26vIXtBrjDC5wuZ7Mpujhf.aqrEWiQr78HA0IwusfXMVcwbY.S', 1, '2021-06-30 17:00:00', '2021-06-30 17:00:00', 'admin', '', 1, 1),
(2, 'F20113', 'f20113', 'f20113@gmail.com', '$2y$08$7iRzjdyaT3zeFsnQAlvoKuqcvvliL6Igp8nLCywrd9DJCYo3KBc82', 1, '2021-07-06 22:22:58', '2021-07-06 22:22:58', 'user', '', 1, 1),
(3, 'F20117', 'f20117', 'f20117@gmail.com', '$2y$08$/Cx.6.qFV/ts7jxIsJbkMO46mCK/dAuld701BBHpzqgGW.MUVegs2', 1, '2021-07-06 22:23:32', '2021-07-06 22:23:32', 'user', '', 1, 1);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_ibfk_2` FOREIGN KEY (`schedule_id`) REFERENCES `schedule` (`schedule_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `schedule`
--
ALTER TABLE `schedule`
  ADD CONSTRAINT `schedule_ibfk_1` FOREIGN KEY (`train_id`) REFERENCES `train` (`train_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `train`
--
ALTER TABLE `train`
  ADD CONSTRAINT `train_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
