<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_Auth');
	}

	public function index()
	{
    $data['sidebar'] = 'dashboard';
    // var_dump($this->session->userdata());
    // die;
    $this->load->view('dashboard/dashboard', $data);
	}
}
