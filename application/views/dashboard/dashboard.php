<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('layout/head.php') ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <?php $this->load->view('layout/navbar.php') ?>

    <?php $this->load->view('layout/sidebar.php') ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper mt-3">

      <!-- Main content -->
      <section class="content">
        <div class="container">
          <div class="card">
            <div class="card-body">
              <div class="container-fluid">

                <div class="col-lg-12 col-9">
                  <div class="container">
                    <h1 class="display-4 text-center"><b>Selamat Datang!</b></h1>
                    <p class="lead text-center">Sistem Penjadwalan Keberangkatan Kereta Api</p>
                  </div>
                </div>
                <!-- Small boxes (Stat box) -->

              </div><!-- /.container-fluid -->
            </div>
          </div>
        </div>
      </section>
      <!-- /.content -->
    </div>


    <?php $this->load->view('layout/footer.php') ?>

  </div>
  <!-- ./wrapper -->


  <?php $this->load->view('layout/javascript.php') ?>
</body>
</html>
