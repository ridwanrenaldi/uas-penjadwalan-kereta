  <div class="modal fade" id="modal-notif">
    <div class="modal-dialog">
      <div class="modal-content <?php if(isset($notif) && $notif['status'] == 'success'){ echo 'bg-success'; }else{ echo 'bg-warning'; }?>">
        <div class="modal-header">
          <h4 class="modal-title"><?php if(isset($notif) && $notif['status'] == 'success'){ echo 'Succes..!'; }else{ echo 'Warning..!'; }?></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <?php
            if (isset($notif)) {
              echo $notif['message'];
            }
          ?>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->