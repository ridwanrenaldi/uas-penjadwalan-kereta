<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('layout/head.php') ?>
</head>
<body class="hold-transition login-page">

  <div class="modal fade" id="modal-denied">
    <div class="modal-dialog">
      <div class="modal-content bg-danger">
        <div class="modal-header">
          <h4 class="modal-title">Warning..!</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Halaman ini bukan untuk anda, silahkan login dengan akun lain
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-warning" onclick="window.history.back();">BACK</button>
          <a href="<?= site_url('auth/logout') ?>">
            <button type="button" class="btn btn-success">LOGIN</button>
          </a>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

<?php $this->load->view('layout/javascript.php') ?>

<script>
  $(document).ready(function(){
    $("#modal-denied").modal('show');
  });
</script>

</body>
</html>
  
  


