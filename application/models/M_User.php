<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_User extends CI_Model {
  private $table = 'user';
  private $primary = 'user_id';
  
  public function __construct(){
      parent::__construct();
      $this->load->model('M_File');
  }

  public function rules() {
    $post = $this->input->post();

    $rules = array();
    if (isset($post['_name_'])) {
      $rules[] = array( 'field' => '_name_',
                        'label' => 'Name',
                        'rules' => 'required|trim|alpha_numeric_spaces|min_length[4]|max_length[20]');
    }
    
    if (isset($post['_username_'])) {
      $rules[] = array( 'field' => '_username_',
                        'label' => 'Username',
                        'rules' => 'required|trim|alpha_numeric|min_length[4]|max_length[12]|is_unique[user.user_username]',
                        'errors' => array('is_unique' => 'This %s already exists.'));
    }

    if (isset($post['_email_'])) {
      $rules[] = array( 'field' => '_email_',
                        'label' => 'Email',
                        'rules' => 'required|trim|valid_email|is_unique[user.user_email]');
    }

    if (isset($post['_password_'])) {
      $rules[] = array( 'field' => '_password_',
                        'label' => 'Password',
                        'rules' => 'required|trim|alpha_numeric|min_length[4]|max_length[20]');
    }

    if (isset($post['_passconf_'])) {
      $rules[] = array( 'field' => '_passconf_',
                        'label' => 'Confirm Password',
                        'rules' => 'required|trim|matches[_password_]');
    }

    if (isset($post['_role_'])) {
      $rules[] =  array( 'field' => '_role_',
                        'label' => 'Role',
                        'rules' => 'required|trim|alpha|in_list[admin,user]');
    }

    return $rules;
  }

  public function getById($id){
    return $this->db->get_where($this->table, array($this->primary => $id) )->row_array();
  }

  public function getAll() {
    return $this->db->get($this->table)->result_array();
  }

  public function insert(){
    $post = $this->input->post();
    if (!empty($post)){

      $check_username = $this->db->get_where($this->table, array('user_username' => strtolower(htmlspecialchars($post['_username_']))))->num_rows();
      $check_email = $this->db->get_where($this->table, array('user_email' => strtolower(htmlspecialchars($post['_email_']))))->num_rows();
      
      if ($check_username > 0 || $check_email > 0) {

        if ($check_username > 0) {
          $response = array(
            'status' => 'error',
            'message' => 'This username already exists',
          );
        } else {
          $response = array(
            'status' => 'error',
            'message' => 'This email already exists',
          );
        }

      } else {

        if ($post['_password_'] === $post['_passconf_']) {

          $uploadimg = $this->M_File->upload_file('./uploads/user/','jpg|png|jpeg|');

          if ($uploadimg['status'] == 'error') {
            $response = array(
              'status' => 'error',
              'message' => $uploadimg['message'],
            );

          } else {
            $data = array(
              'user_id'           => NULL,
              'user_name'         => htmlspecialchars($post['_name_']),
              'user_password'     => password_hash(strtolower($post['_password_']), PASSWORD_BCRYPT, array('cost'=>8)),
              'user_isactive'     => 1,
              'user_created'      => date('Y-m-d H:i:s'),
              'user_modified'     => date('Y-m-d H:i:s'),
              'user_role'         => 'admin',
              'user_image'        => NULL,
              'user_isupdate'     => htmlspecialchars($post['_isupdate_']),
              'user_isdelete'     => htmlspecialchars($post['_isdelete_']),
            );

            if ($uploadimg['status'] == 'success') {
              $data['user_image'] = $uploadimg['data']['file_name'];
            }

            if (isset($post['_username_'])) {
              $data['user_username'] = strtolower(htmlspecialchars($post['_username_']));
            }

            if (isset($post['_email_'])) {
              $data['user_email'] = strtolower(htmlspecialchars($post['_email_']));
            }

            if (isset($post['_role_'])) {
              $data['user_role'] = strtolower(htmlspecialchars($post['_role_']));
            }

            $data = $this->security->xss_clean($data);
            if($this->db->insert($this->table, $data)){
              $response = array(
                'status' => 'success',
                'message' => 'Success insert data',
              );
            } else {
              $response = array(
                'status' => 'error',
                'message' => 'Failed insert data',
              );
            }
          } 
        } else {
          $response = array(
            'status' => 'error',
            'message' => 'Confirm password must be correct!',
          );
        }
      }
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data not found!',
      );
    }

    return $response;
  }

  public function update($id){
    $post = $this->input->post();
    if (!empty($post)){

      if (!empty($post['_username_'])) {
        $check = $this->db->get_where($this->table, array('user_username' => strtolower(htmlspecialchars($post['_username_']))))->num_rows();
        if ($check > 0) {
          $username = array(
            'status' => 'error',
            'message' => 'This username already exists',
          );
        } else {
          $username = TRUE;
        }
      } else {
        $username = TRUE;
      }


      if (!empty($post['_email_'])) {
        $check = $this->db->get_where($this->table, array('user_email' => strtolower(htmlspecialchars($post['_email_']))))->num_rows();
        if ($check > 0) {
          $username = array(
            'status' => 'error',
            'message' => 'This email already exists',
          );
        } else {
          $email = TRUE;
        }
      } else {
        $email = TRUE;
      }


      if (!empty($post['_password_']) && !empty($post['_passconf_'])) {
        if ($post['_password_'] === $post['_passconf_']) {
          $password = TRUE;
        } else {
          $password = array(
            'status' => 'error',
            'message' => 'Confirm password must be correct!',
          );
        }
      } else {
        $password = TRUE;
      }

      
      if ($username == TRUE && $email == TRUE && $password == TRUE) {

        $user = $this->getById($id);
        if (!empty($user['user_image']) && $user['user_image'] != 'default.png') {
          $oldpath = './uploads/user/'.$user['user_image'];
          $this->M_File->delete_file($oldpath);
        }
         
        $uploadimg = $this->M_File->upload_file('./uploads/user/','jpg|png|jpeg|');
        if ($uploadimg['status'] == 'error') {
          $response = $uploadimg;

        } else {
          
          $data = array(
            'user_name'         => htmlspecialchars($post['_name_']),
            'user_modified'     => date('Y-m-d H:i:s'),
            'user_isupdate'     => htmlspecialchars($post['_isupdate_']),
            'user_isdelete'     => htmlspecialchars($post['_isdelete_']),
          );

          if ($uploadimg['status'] == 'success') {
            $data['user_image'] = $uploadimg['data']['file_name'];
          }

          if (isset($post['_username_'])) {
            $data['user_username'] = strtolower(htmlspecialchars($post['_username_']));
          }

          if (isset($post['_email_'])) {
            $data['user_email'] = strtolower(htmlspecialchars($post['_email_']));
          }

          if (isset($post['_password_']) && isset($post['_passconf_'])) {
            $data['user_password'] = password_hash(strtolower($post['_password_']), PASSWORD_BCRYPT, array('cost'=>8));
          }

          if (isset($post['_role_'])) {
            $data['user_role'] = strtolower(htmlspecialchars($post['_role_']));
          }

          $data = $this->security->xss_clean($data);
          $this->db->where($this->primary, $id);
          if($this->db->update($this->table, $data)){
            $response = array(
              'status' => 'success',
              'message' => 'Success update data',
            );
          } else {
            $response = array(
              'status' => 'error',
              'message' => 'Failed update data',
            );
          }
        }
      } else {
        if ($username != TRUE) {
          $response = $username;
        } elseif ($email != TRUE) {
          $response = $email;
        } else {
          $response = $password;
        }
      }
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data not found!',
      );
    }
    return $response;
  }

  public function delete($id){
    $data = $this->getById($id);
    if ($data) {
      
      $path = './uploads/user/'.$data['user_image'];
      if (!empty($data['user_image']) && $data['user_image'] != 'default.png') {
        $this->M_File->delete_file($path);
      }

      if($this->db->delete($this->table, array($this->primary => $id))){
        $response = array(
          'status' => 'success',
          'message' => 'Success delete data',
        );

      } else {
        $response = array(
          'status' => 'error',
          'message' => 'Failed delete data',
        );
      }

    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data not found',
      );
    }
    return $response;
  }

}
?>