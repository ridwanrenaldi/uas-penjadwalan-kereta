<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Sales extends CI_Model {
  private $table = 'sales';
  private $primary = 'sales_id';
  
  public function __construct(){
      parent::__construct();
      $this->load->model('M_File');
  }

  public function rules() {
    $rules = array(
      array( 'field' => '_user_',
              'label' => 'User',
              'rules' => 'required|trim|numeric|min_length[1]|max_length[11]'),

      array( 'field' => '_schedule_',
              'label' => 'Schedule',
              'rules' => 'required|trim|numeric|min_length[1]|max_length[11]'),

      array( 'field' => '_person_',
              'label' => 'Person',
              'rules' => 'required|trim|numeric|min_length[1]|max_length[11]'),

      // array( 'field' => '_price_',
      //         'label' => 'Price',
      //         'rules' => 'required|trim|numeric|min_length[1]|max_length[11]'),

      // array( 'field' => '_total_',
      //         'label' => 'Total',
      //         'rules' => 'required|trim|numeric|min_length[1]|max_length[11]'),

    );

    return $rules;
  }

  public function getById($id){
    return $this->db->get_where($this->table, array($this->primary => $id) )->row_array();
  }

  public function getAll() {
    $this->db->from('sales, user, schedule');
    $this->db->where('user.user_id = sales.user_id');
    $this->db->where('schedule.schedule_id = sales.schedule_id');
    return $this->db->get()->result_array();
  }

  public function insert(){
    $post = $this->input->post();
    if (!empty($post)){
      if($this->session->userdata('typeusername') == 'genap'){
        $folder = 'berkas_genap';
        $uploadfile = $this->M_File->upload_file('./uploads/berkas_genap', 'pdf');

      } elseif ($this->session->userdata('typeusername') == 'ganjil') {
        $folder = 'berkas_ganjil';
        $uploadfile = $this->M_File->upload_file('./uploads/berkas_ganjil', 'doc|docx');

      } else {
        $uploadfile = array(
          'status' => 'empty',
          'message' => 'Choose file to upload'
        );
      }


      if ($uploadfile['status'] == 'error') {
        $response = $uploadfile;
      } else {
        $schedule = $this->M_Schedule->getById(htmlspecialchars($post['_schedule_']));
        $data = array(
          'sales_id'           => NULL,
          'user_id'            => htmlspecialchars($post['_user_']),
          'schedule_id'        => $schedule['schedule_id'],
          'sales_person'       => htmlspecialchars($post['_person_']),
          'sales_price'        => $schedule['train_price'],
          'sales_total'        => $schedule['train_price'] * htmlspecialchars($post['_person_']),
          'sales_created'      => date('Y-m-d H:i:s'),
          'sales_modified'     => date('Y-m-d H:i:s'),
          'sales_file'         => NULL,
        );

        if ($uploadfile['status'] == 'success') {
          $data['sales_file'] = $folder.'/'.$uploadfile['data']['file_name'];
        }
  
        $data = $this->security->xss_clean($data);
        if($this->db->insert($this->table, $data)){
          $response = array(
            'status' => 'success',
            'message' => 'Success insert data',
          );
        } else {
          $response = array(
            'status' => 'error',
            'message' => 'Failed insert data',
          );
        }
      }
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data not found!',
      );
    }

    return $response;
  }

  public function update($id){
    $post = $this->input->post();
    if (!empty($post)){
      if($this->session->userdata('typeusername') == 'genap'){
        $folder = 'berkas_genap';
        $uploadfile = $this->M_File->upload_file('./uploads/berkas_genap', 'pdf');

      } elseif ($this->session->userdata('typeusername') == 'ganjil') {
        $folder = 'berkas_ganjil';
        $uploadfile = $this->M_File->upload_file('./uploads/berkas_ganjil', 'doc|docx');

      } else {
        $uploadfile = array(
          'status' => 'empty',
          'message' => 'Choose file to upload'
        );
      }

      if ($uploadfile['status'] == 'error') {
        $response = $uploadfile;

      } else {
        $schedule = $this->M_Schedule->getById(htmlspecialchars($post['_schedule_']));
        $data = array(
          'user_id'            => htmlspecialchars($post['_user_']),
          'schedule_id'        => $schedule['schedule_id'],
          'sales_person'       => htmlspecialchars($post['_person_']),
          'sales_price'        => $schedule['train_price'],
          'sales_total'        => $schedule['train_price'] * htmlspecialchars($post['_person_']),
          'sales_modified'     => date('Y-m-d H:i:s'),
        );

        if ($uploadfile['status'] == 'success') {
          $data['sales_file'] = $folder.'/'.$uploadfile['data']['file_name'];

          $getdata = $this->getById($id);
          $this->M_File->delete_file('./uploads/'.$getdata['sales_file']);
        }
  
        $data = $this->security->xss_clean($data);
        $this->db->where($this->primary, $id);
        if($this->db->update($this->table, $data)){
          $response = array(
            'status' => 'success',
            'message' => 'Success update data',
          );
        } else {
          $response = array(
            'status' => 'error',
            'message' => 'Failed update data',
          );
        }
      }
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data not found!',
      );
    }
    return $response;
  }

  public function delete($id){
    $data = $this->getById($id);
    if ($data) {

      $path = './uploads/'.$data['sales_file'];
      if (!empty($data['sales_file'])) {
        $this->M_File->delete_file($path);
      }

      if($this->db->delete($this->table, array($this->primary => $id))){
        $response = array(
          'status' => 'success',
          'message' => 'Success delete data',
        );
        
      } else {
        $response = array(
          'status' => 'error',
          'message' => 'Failed delete data',
        );
      }

    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data not found',
      );
    }

    return $response;
  }

}
?>