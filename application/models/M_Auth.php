<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Auth extends CI_Model {
  private $table = 'user';
  
  public function __construct(){
      parent::__construct();
      $this->load->model('M_File');
  }

  public function rules_login(){
    $rules = array(

      array(  'field' => '_username_',
              'label' => 'Username',
              'rules' => 'required|trim|alpha_numeric|min_length[4]|max_length[12]'),

      array(  'field' => '_password_',
            'label' => 'Password',
            'rules' => 'required|trim|alpha_numeric|min_length[4]|max_length[20]'),
    );
    
    return $rules;
  }

  public function rules_register(){
    $post = $this->input->post();

    $rules = array(
      array( 'field' => '_name_',
              'label' => 'Name',
              'rules' => 'required|trim|alpha_numeric_spaces|min_length[4]|max_length[20]'),

      array( 'field' => '_username_',
              'label' => 'Username',
              'rules' => 'required|trim|alpha_numeric|min_length[4]|max_length[12]|is_unique[user.user_username]',
              'errors' => array('is_unique' => 'This %s already exists.')),

      array( 'field' => '_email_',
              'label' => 'Email',
              'rules' => 'required|trim|valid_email|is_unique[user.user_email]'),

      array( 'field' => '_password_',
              'label' => 'Password',
              'rules' => 'required|trim|alpha_numeric|min_length[4]|max_length[20]'),

      array( 'field' => '_passconf_',
              'label' => 'Confirm Password',
              'rules' => 'required|trim|matches[_password_]'),
    );

    return $rules;
  }

  public function check_usename($username){
    $lastchar = substr($username, -2);
    if (is_numeric($lastchar)) {
      if ($lastchar % 2 == 0) {
        $response = 'genap';
      } else {
        $response = 'ganjil';
      }
    } else {
      $response = FALSE;
    }

    return $response;
  }

  public function login() {
    $post = $this->input->post();
    if (!empty($post['_username_']) && !empty($post['_password_'])) {
      
      $getuser = $this->db->get_where($this->table, array('user_username' => strtolower($post['_username_'])));
      if ($getuser->num_rows() > 0) {

        $user = $getuser->row_array();
        if ($user['user_isactive']) {

          if (password_verify(strtolower($post['_password_']), $user['user_password'])) {
            $data = [
              'id'        => $user['user_id'],
              'name'      => $user['user_name'],
              'username'  => $user['user_username'],
              'email'     => $user['user_email'],
              'role'      => $user['user_role'],
              'image'     => $user['user_image'],
              'isupdate'  => $user['user_isupdate'],
              'isdelete'  => $user['user_isdelete'],
              'typeusername' => $this->check_usename($user['user_name']),
            ];
            
            $this->session->set_userdata($data);
            $response = array(
              'status'    => 'success',
              'message'   => 'Login successful!',
              'role'      => $user['user_role'],
              'username'  => $user['user_username']
            );

          } else {
            $response = array(
              'status' => 'error',
              'message' => 'Wrong password!',
            );
          }
        } else {
          $response = array(
            'status' => 'error',
            'message' => 'This username has not been activated!',
          );
        }
      } else {
        $response = array(
          'status' => 'error',
          'message' => 'Username not found!',
        );
      }

    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Enter data correctly!',
      );
    }
    

    return $response;
  }

  public function register(){
    $post = $this->input->post();
    if (!empty($post)){

      $check_username = $this->db->get_where($this->table, array('user_username' => strtolower(htmlspecialchars($post['_username_']))))->num_rows();
      $check_email = $this->db->get_where($this->table, array('user_email' => strtolower(htmlspecialchars($post['_email_']))))->num_rows();
      
      if ($check_username > 0 || $check_email > 0) {

        if ($check_username > 0) {
          $response = array(
            'status' => 'error',
            'message' => 'This username already exists',
          );
        } else {
          $response = array(
            'status' => 'error',
            'message' => 'This email already exists',
          );
        }

      } else {

        if ($post['_password_'] === $post['_passconf_']) {

          $uploadimg = $this->M_File->upload_file('./uploads/user/','jpg|png|jpeg|');

          if ($uploadimg['status'] == 'error') {
            $response = array(
              'status' => 'error',
              'message' => $uploadimg['message'],
            );

          } else {
            $data = array(
              'user_id'           => NULL,
              'user_name'         => htmlspecialchars($post['_name_']),
              'user_password'     => password_hash(strtolower($post['_password_']), PASSWORD_BCRYPT, array('cost'=>8)),
              'user_isactive'     => 1,
              'user_created'      => date('Y-m-d H:i:s'),
              'user_modified'     => date('Y-m-d H:i:s'),
              'user_role'         => 'user',
              'user_image'        => NULL,
              'user_isupdate'     => 1,
              'user_isdelete'     => 1,
            );

            if ($uploadimg['status'] == 'success') {
              $data['user_image'] = $uploadimg['data']['file_name'];
            }

            if (isset($post['_username_'])) {
              $data['user_username'] = strtolower(htmlspecialchars($post['_username_']));
            }

            if (isset($post['_email_'])) {
              $data['user_email'] = strtolower(htmlspecialchars($post['_email_']));
            }

            if (isset($post['_role_'])) {
              $data['user_role'] = strtolower(htmlspecialchars($post['_role_']));
            }

            $data = $this->security->xss_clean($data);
            if($this->db->insert($this->table, $data)){
              $response = array(
                'status' => 'success',
                'message' => 'Successfully registered, please login',
              );
            } else {
              $response = array(
                'status' => 'error',
                'message' => 'Failed to register, please try again',
              );
            }
          } 
        } else {
          $response = array(
            'status' => 'error',
            'message' => 'Confirm password must be correct!',
          );
        }
      }
    } else {
      $response = array(
        'status' => 'error',
        'message' => 'Data not found!',
      );
    }

    return $response;
  }

  public function check_session($roles=array()){
    $session = $this->session->userdata();
    if (!empty($session)) {
      if (!empty($session['id']) && !empty($session['name']) && !empty($session['username']) && !empty($session['email']) && !empty($session['role'])) {
        if (in_array($session['role'], $roles, TRUE)) {
          $data = [
            'id'        => $session['id'],
            'name'      => $session['name'],
            'username'  => $session['username'],
            'email'     => $session['email'],
            'role'      => $session['role'],
            'image'     => $session['image'],
            'isupdate'  => $session['isupdate'],
            'isdelete'  => $session['isdelete'],
            'typeusername' => $session['typeusername'],
          ];
          return $data;
        } else {
          return FALSE;
        }
      } else {
        return FALSE;
      }
    } else {
      return FALSE;
    }
  }

}
?>