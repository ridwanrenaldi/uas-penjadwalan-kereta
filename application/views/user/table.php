<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('layout/head.php') ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <?php $this->load->view('layout/navbar.php') ?>

    <?php $this->load->view('layout/sidebar.php') ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Main content -->
      <section class="content mt-3">
        <div class="row">
          <div class="col-12">

            <div class="card card-default">

              <div class="card-header">
                <h3 class="card-title">Data Users</h3>
                <div class="card-tools">
                  <ul class="nav nav-pills ml-auto">
                    <li class="nav-item">
                      <a href="<?= site_url('F20113/user_add') ?>">
                        <button type="button" class="btn btn-secondary btn-sm"><i class="fa fa-plus-square"></i> Add User</button>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>

              <div class="card-body">
                <table id="_table_" class="table table-bordered table-hover table-striped" width="100%">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Is Active</th>
                    <th>Role</th>
                    <th>Image</th>
                    <th>Is Update</th>
                    <th>Is Delete</th>
                    <th>Created</th>
                    <th>Modified</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($data as $key => $value) { ?>
                      <tr>
                        <td><?= $key + 1 ?></td>
                        <td><?= $value['user_name'] ?></td>
                        <td><?= $value['user_username'] ?></td>
                        <td><?= $value['user_email'] ?></td>
                        <td>
                          <?php
                            if($value['user_isactive']){
                              echo '<span class="badge badge-success"><i class="fas fas fa-check-circle"></i></span><span style="color: #fff0;">'.$value['user_isactive'].'</span>';
                            } else {
                              echo '<span class="badge badge-danger"><i class="fas fa-times-circle"></i></span><span style="color: #fff0;">'.$value['user_isactive'].'</span>';
                            } 
                          ?>
                        </td>
                        <td><?= $value['user_role'] ?></td>
                        
                        <td>
                          <?php
                            if($value['user_image']){
                              echo '<img src="'.base_url('uploads/user/'.$value['user_image']).'" alt="Image" width="50px" height="50px">';
                            } else {
                              echo '<img src="'.base_url('assets/images/default.png').'" alt="Image" width="50px" height="50px">';
                            }
                            
                          ?>
                        </td>
                        <td>
                          <?php
                            if($value['user_isupdate']){
                              echo '<span class="badge badge-success"><i class="fas fas fa-check-circle"></i></span><span style="color: #fff0;">'.$value['user_isupdate'].'</span>';
                            } else {
                              echo '<span class="badge badge-danger"><i class="fas fa-times-circle"></i></span><span style="color: #fff0;">'.$value['user_isupdate'].'</span>';
                            } 
                          ?>
                        </td>
                        <td>
                          <?php
                            if($value['user_isdelete']){
                              echo '<span class="badge badge-success"><i class="fas fas fa-check-circle"></i></span><span style="color: #fff0;">'.$value['user_isdelete'].'</span>';
                            } else {
                              echo '<span class="badge badge-danger"><i class="fas fa-times-circle"></i></span><span style="color: #fff0;">'.$value['user_isdelete'].'</span>';
                            } 
                          ?>
                        </td>
                        <td><?= $value['user_created'] ?></td>
                        <td><?= $value['user_modified'] ?></td>
                        <td>
                          <?php if ($this->session->userdata('isupdate')) { ?>
                            <a href="<?= site_url('F20113/user_edit/'.$value['user_id'])?>" data-toggle="tooltip" title="Edit">
                                <button type="button" class="btn btn-warning"><i class="fa fa-edit"></i></button>
                            </a>
                          <?php } if ($this->session->userdata('isdelete')) { ?>
                            <a onclick="deletedata(<?= $value['user_id'] ?>)" data-toggle="tooltip" title="Delete">
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-trash"></i></button>
                            </a>
                          <?php } ?>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->

    </div>


    <?php $this->load->view('layout/footer.php') ?>

  </div>
  <!-- ./wrapper -->

  <!-- modal notification -->
  <?php $this->load->view('layout/notif.php') ?>

  <!-- modal delete -->
  <?php $this->load->view('layout/delete.php') ?>

  <?php $this->load->view('layout/javascript.php') ?>
  <script>

    var title = 'Data User';
    var columns = [0,1,2,3,4,5,6,7,8,9,10];
    var filename = 'Export Data User';

    function deletedata(id){
      $("#btn-delete").on("click", function(){
        window.location.href = "<?= site_url('F20113/user_delete/')?>"+id;
      });
    }

    $(document).ready(function(){

      <?php if (isset($notif) && !empty($notif['status']) && !empty($notif['message'])) { ?>
        $("#modal-notif").modal('show');
      <?php } ?>


      $("#_table_").DataTable({
        scrollX : true,
        columnDefs: [
            { className: "tabledata", targets: "_all"}
        ],
        dom:  '<"row"<"col-sm-12 col-md-3"l><"col-sm-12 col-md-6"B><"col-sm-12 col-md-3"f>>' +
              '<"row"<"col-sm-12"tr>>' +
              '<"row"<"col-sm-12 col-md-5"i><"col-sm-12 col-md-7"p>>',
        buttons: [
          {
            extend      : 'excel',
            title       : title,
            text        : '<i class="far fa-file-excel"></i> Excel',
            titleAttr   : 'Excel',
            className   : 'btn btn-default btn-sm',
            exportOptions: {
              columns: columns
            },
            filename: filename,
            footer: true,
          },
          {
            extend      : 'pdf',
            title       : title,
            oriented    : 'potrait',
            pageSize    : 'LEGAL',
            // download    : 'open',
            text        : '<i class="far fa-file-pdf"></i> PDF',
            titleAttr   : 'PDF',
            className   : 'btn btn-default btn-sm',
            exportOptions: {
              columns: columns
            },
            filename: filename,
            footer: true,
          },               
        ],
      });
    });
    
  </script>
</body>
</html>
